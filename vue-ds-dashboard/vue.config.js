module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'Vue Data Science Dashboard'
    }
  },
  devServer: {
    disableHostCheck: true
  }
}
